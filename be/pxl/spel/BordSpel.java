package be.pxl.spel;
import java.util.Formatter;

public class BordSpel extends Spel{
	public enum BordSpellen {
		KING_OF_TOKYO("King of Tokyo", new Uitgever(Uitgever.Uitgevers.IELLO), 30, 6, 8),
		CLUEDO("Cluedo", new Uitgever(Uitgever.Uitgevers.HASBRO), 60, 6, 8),
		DEAD_OF_WINTER("Dead of Winter", new Uitgever(Uitgever.Uitgevers.PLAID_HAT_GAMES), 180, 5, 13),
		MONOPOLY("Monopoly", new Uitgever(Uitgever.Uitgevers.HASBRO), 120, 6, 8);
		
		private final String NAAM;
		private final Uitgever UITGEVER;
		private final int SPEELDUUR;
		private final int AANTAL_SPELERS;
		private final int MINIMUM_LEEFTIJD;
		
		BordSpellen(String naam, Uitgever uitgever, int speelduur
				, int aantalSpelers, int minimumLeeftijd) {
			NAAM = naam;
			UITGEVER = uitgever;
			SPEELDUUR = speelduur;
			AANTAL_SPELERS = aantalSpelers;
			MINIMUM_LEEFTIJD = minimumLeeftijd;
		}
	}
	
	private int speelduur;
	private int aantalSpelers;
	private static final int WAARDE_NEGATIEF_AANTAL_SPELERS = 1;
	private static final int WAARDE_NEGATIEVE_SPEELDUUR = 30;
	
	public BordSpel(String naam, Uitgever uitgever) {
		super(naam, uitgever);
	}
	
	public BordSpel(BordSpellen gekend) {
		this(gekend.NAAM, gekend.UITGEVER);
		setSpeelduur(gekend.SPEELDUUR);
		setAantalSpelers(gekend.AANTAL_SPELERS);
		super.setMinimumLeeftijd(gekend.MINIMUM_LEEFTIJD);
	}

	public int getSpeelduur() {
		return speelduur;
	}

	public int getAantalSpelers() {
		return aantalSpelers;
	}

	public void setSpeelduur(int speelduur) {
		if(speelduur < 0) {
			speelduur = WAARDE_NEGATIEVE_SPEELDUUR;
		}
		this.speelduur = speelduur;
	}

	public void setAantalSpelers(int aantalSpelers) {
		if(aantalSpelers < 0) {
			aantalSpelers = WAARDE_NEGATIEF_AANTAL_SPELERS;
		}
		this.aantalSpelers = aantalSpelers;
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		Formatter formatter = new Formatter(output);
		
		formatter.format("%s{naam='%s', minimumLeeftijd=%d,%nuitgever=%s,%naantalSpelers=%d, speelDuur=%d}",
				this.getClass().getSimpleName(), super.getNaam(), super.getMinimumLeeftijd(),
				super.getUitgever().toString(), aantalSpelers, speelduur);
		
		formatter.close();
		
		return output.toString();
	}
	
}
