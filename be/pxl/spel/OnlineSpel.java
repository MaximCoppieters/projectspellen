package be.pxl.spel;
import java.util.Formatter;

public class OnlineSpel extends Spel{
	
	private String genre;
	private String url;
	
	public OnlineSpel(String naam, Uitgever uitgever) {
		super(naam, uitgever);
	}

	public String getGenre() {
		return genre;
	}

	public String getUrl() {
		return url;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		Formatter formatter = new Formatter(output);
		
		formatter.format("%s{naam='%s', minimumLeeftijd=%d,%nuitgever=%s,%ngenre=%s, url=%s",
				this.getClass().getSimpleName(), super.getNaam(), super.getMinimumLeeftijd(),
				super.getUitgever().toString(), genre, url);
		
		formatter.close();
		
		return output.toString();
	}
}
