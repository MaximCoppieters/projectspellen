package be.pxl.spel;

public class Score {
	public static final int MAX_SCORE=5;
	private int waarde;
	
	public Score(int waarde) {
		setWaarde(waarde);
	}
	
	public Score(Score[] scores) {
		this.waarde = berekenGemiddeldeScore(scores);
	}

	private int berekenGemiddeldeScore(Score[] scores) {
		if(scores.length == 0) return 0;
		//Deling door 0 voorkomen
		int totaalScore = 0;
		
		for(Score score : scores) {
			totaalScore += score.waarde;
		}
		
		totaalScore = totaalScore / scores.length;
		
		return totaalScore;
	}
	
	public void setWaarde(int waarde) {
		waarde = Math.min(waarde, MAX_SCORE);
		waarde = Math.max(waarde, 0);
		
		this.waarde = waarde;
	}
	
	public String getStars() {
		StringBuilder sterren = new StringBuilder();
		for(int i=0; i < waarde; i++) {
			sterren.append("*");
		}
		return sterren.toString();
	}
	
	public boolean equals(Object o) { //ontvangen als object van Object om foutmelding instanceof te voorkomen
		if(o == null) return false;
		if(!(o instanceof Score)) return false;
		// uitsluiten objecten van andere klassen
		Score s = (Score) o;
		if(s.waarde == this.waarde) {
			return true;
		}
		
		return false;
	}
	
	
}
