package be.pxl.spel;

public abstract class Spel {
	private String naam;
	private int minimumLeeftijd;
	private Uitgever uitgever;
	
	public Spel(String naam, Uitgever uitgever) {
		this.naam = naam;
		this.uitgever = uitgever;
	}

	public String getNaam() {
		return naam;
	}

	public int getMinimumLeeftijd() {
		return minimumLeeftijd;
	}

	public Uitgever getUitgever() {
		return uitgever;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public void setMinimumLeeftijd(int minimumLeeftijd) {
		if(minimumLeeftijd < 0) {
			minimumLeeftijd = 12;
		}
		this.minimumLeeftijd = minimumLeeftijd;
	}
	
	public void setUitgever(Uitgever uitgever) {
		this.uitgever = uitgever;
	}

	public abstract String toString();
}
