package be.pxl.spel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import be.pxl.spel.BordSpel.BordSpellen;
import be.pxl.spel.Uitgever.Uitgevers;

public class SpelApp {
	private static Scanner toetsenbord = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Map<String, BordSpel> bordspellenPerNaam = new HashMap<>();
		Score[] scores;
		int scoresIndex = 0;
		List<SpelEvaluatie> evaluaties = new ArrayList<>();
		
		BordSpel bordSpel;
		String naamGekozenBordSpel;
		int aantalSpelers;
		String naamHuidigeSpeler;
		int leeftijdHuidigeSpeler;
		SpelEvaluatie evaluatie;
		int scoreWaarde;
		String motivatie;
		Score gemiddeldeScore;
		
		voegStandaardSpellenToe(bordspellenPerNaam);
		
		System.out.println("Welk bordspel wilt u beoordelen?");
		System.out.println("Gekende bordspellen: ");
		printGekendeNamen(bordspellenPerNaam);
		naamGekozenBordSpel = toetsenbord.nextLine();
		
		if(bordspellenPerNaam.get(naamGekozenBordSpel) == null) {
			System.out.println("Het opgegeven bordspel bestaat nog niet,"
					+ " we voegen het nu toe adhv. uw gegevens");
			voegNieuwBordSpelToe(naamGekozenBordSpel, bordspellenPerNaam);	
		}
		
		bordSpel = bordspellenPerNaam.get(naamGekozenBordSpel);
		
		System.out.println(bordSpel);
		
		System.out.println("Hoeveel spelers evalueren het spel?");
		aantalSpelers = toetsenbord.nextInt();
		scores = new Score[aantalSpelers];
		bordSpel.setAantalSpelers(aantalSpelers);
		
		toetsenbord.nextLine(); //linefeed
		
		for(int i=1; i <= bordSpel.getAantalSpelers(); i++) {
			System.out.println("Geef naam van speler " + i);
			naamHuidigeSpeler = toetsenbord.nextLine();
			System.out.println("Geef het geboortejaar van de speler");
			leeftijdHuidigeSpeler = toetsenbord.nextInt();
			System.out.println("Geef uw score (0-5)");
			scoreWaarde = toetsenbord.nextInt();
			toetsenbord.nextLine(); //linefeed
			System.out.println("Geef uw motivatie");
			motivatie = toetsenbord.nextLine();
			
			evaluatie = new SpelEvaluatie(bordSpel, new Speler(naamHuidigeSpeler, leeftijdHuidigeSpeler));
			evaluatie.maakBeoordeling(scoreWaarde, motivatie);
			
			evaluaties.add(evaluatie);
		}
		
		for(SpelEvaluatie spelEvaluatie : evaluaties) {
			char aanduidingGeldigheid = spelEvaluatie.isGeldig() ? '+' : '-';
			
			System.out.printf("%-12s %5s %-15s %c%n",
					spelEvaluatie.getSpeler().getNaam(), spelEvaluatie.getScore().getStars(),
					spelEvaluatie.getMotivatie(), aanduidingGeldigheid);
			
			scores[scoresIndex++] = spelEvaluatie.getScore();
		}
		
		gemiddeldeScore = new Score(scores);
		
		System.out.println(gemiddeldeScore.getStars());
		
		toetsenbord.close();
	}
	
	private static void voegNieuwBordSpelToe(String naamBordSpel, Map<String, BordSpel> bordspellenPerNaam) {
		Map<String, Uitgever> uitgeversPerNaam = geefStandaardUitgeversPerNaam();
		String naamUitgever;
		Uitgever uitgever;
		int speelduur;
		int aantalSpelers;
		BordSpel bordSpel;
		
		System.out.println("Wat is de naam van de uitgever");
		System.out.println("Gekende uitgevers:");
		printGekendeNamen(uitgeversPerNaam);
		naamUitgever = toetsenbord.nextLine();
		
		if(uitgeversPerNaam.get(naamUitgever) == null) {
			System.out.println("De opgegeven uitgever bestaat nog niet,"
					+ " we voegen hem nu toe adhv. uw gegevens");
			voegNieuweUitgeverToe(naamUitgever, uitgeversPerNaam);
		}
		
		uitgever = uitgeversPerNaam.get(naamUitgever);

		bordSpel = new BordSpel(naamBordSpel, uitgever);
		
		System.out.println("Wat is de gemiddelde speelduur van het bordspel?");
		speelduur = toetsenbord.nextInt();
		System.out.println("Wat is het maximaal aantal spelers van het spel?");
		aantalSpelers = toetsenbord.nextInt();
		
		bordSpel.setAantalSpelers(aantalSpelers);
		bordSpel.setSpeelduur(speelduur);
		
		bordspellenPerNaam.put(naamBordSpel, bordSpel);
	}
	
	private static void voegNieuweUitgeverToe(String naamUitgever,
			Map<String, Uitgever> uitgeversPerNaam) {
		String hoofdzetel;
		int jaarOprichting;
		
		System.out.println("Waar bevindt de hoofdzetel van de uitgever zich?");
		hoofdzetel = toetsenbord.nextLine();
		
		System.out.println("In welk jaar is het bedrijf opgericht?");
		jaarOprichting = toetsenbord.nextInt();
		
		toetsenbord.nextLine(); //Voedt lijn na opvraag integer
		
		uitgeversPerNaam.put(naamUitgever, new Uitgever(naamUitgever, hoofdzetel, jaarOprichting));
	}
	
	public static void voegStandaardSpellenToe(Map<String, BordSpel> bordspellenPerNaam) {
		for(BordSpellen standaardSpel : BordSpel.BordSpellen.values()) {
			BordSpel bordspel = new BordSpel(standaardSpel);
			bordspellenPerNaam.put(bordspel.getNaam(), bordspel);
		}
	}
	
	public static Map<String, Uitgever> geefStandaardUitgeversPerNaam() {
		Map<String, Uitgever> uitgeversPerNaam = new HashMap<>();
		for(Uitgevers standaardUitgever : Uitgever.Uitgevers.values()) {
			Uitgever uitgever = new Uitgever(standaardUitgever);
			uitgeversPerNaam.put(uitgever.getNaam(), uitgever);
		}
		return uitgeversPerNaam;
	}
	
	public static <T> void printGekendeNamen(Map<String, T> collectiePerNaam) {
		for(String naam : collectiePerNaam.keySet()) {
			System.out.println(naam);
		}
	}
	
}
