package be.pxl.spel;

public class SpelEvaluatie {
	private String motivatie;
	private Spel spel;
	private Speler speler;
	private Score score;
	
	public SpelEvaluatie(Spel spel, Speler speler) {
		this.spel = spel;
		this.speler = speler;
	}
	
	public boolean maakBeoordeling(int scoreWaarde, String motivatie) {
		if(score == null) {
			score = new Score(scoreWaarde);
			this.motivatie = motivatie;
			return true;
		}
		return false;
	}
	
	public boolean isGeldig() {
		if(spel != null && speler != null && score != null) {
			if(spel.getMinimumLeeftijd() <= speler.getLeeftijd()) {
				return true;
			}
		}
		return false;
	}
	
	public Spel getSpel() {
		return spel;
	}
	
	public Speler getSpeler() {
		return speler;
	}
	
	public Score getScore() {
		return score;
	}
	
	public String getMotivatie() {
		return motivatie;
	}
}
