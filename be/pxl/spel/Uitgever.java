package be.pxl.spel;
import java.time.LocalDate;
import java.util.Formatter;

public class Uitgever {
	public enum Uitgevers {
		HASBRO("Hasbro", "Pawtucket, Rhode Island, United States", 1923, "https://www.hasbro.com/"),
		PLAID_HAT_GAMES("Plaid Hat Games", "Midway Rd., Addison, United States", 2009, "https://www.plaidhatgames.com/"),
		IELLO("IELLO", "Nancy, France", 2004, "http://www.iellogames.com/");
		
		private final String NAAM;
		private final int JAAR_OPRICHTING;
		private final String HOOFDKANTOOR;
		private final String WEBSITE;
		
		Uitgevers(String naam, String hoofdkantoor, int jaarOprichting, String website) {
			NAAM = naam;
			HOOFDKANTOOR = hoofdkantoor;
			WEBSITE = website;
			JAAR_OPRICHTING = jaarOprichting;
		}
	}
	private String naam;
	private int jaarOprichting;
	private String hoofdkantoor;
	private String website;
	
	public Uitgever(String naam, String hoofdkantoor, int jaarOprichting) {
		this.naam = naam;
		this.hoofdkantoor = hoofdkantoor;
		setJaarOprichting(jaarOprichting);
	}

	public Uitgever(Uitgevers gekend) {
		this(gekend.NAAM, gekend.HOOFDKANTOOR, gekend.JAAR_OPRICHTING);
		this.website = gekend.WEBSITE;
	}

	public String getNaam() {
		return naam;
	}

	public int getJaarOprichting() {
		return jaarOprichting;
	}

	public String getHoofdkantoor() {
		return hoofdkantoor;
	}

	public String getWebsite() {
		return website;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public void setJaarOprichting(int jaarOprichting) {
		if(jaarOprichting > LocalDate.now().getYear()){
			jaarOprichting = 0;
		}
		this.jaarOprichting = jaarOprichting;
	}

	public void setHoofdkantoor(String hoofdkantoor) {
		this.hoofdkantoor = hoofdkantoor;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	public String toString() {
		StringBuilder output = new StringBuilder();
		Formatter formatter = new Formatter(output);
		
		formatter.format("Uitgever{naam='%s', jaarOprichting=%d,%nHoofdkantoor='%s', website='%s'}", naam, jaarOprichting, hoofdkantoor, website);
		
		formatter.close();
		
		return output.toString();
	}
	
	
}
